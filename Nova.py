from openpyxl import load_workbook
from itertools import combinations, product
from numpy import asarray, repeat


pVsVaisseaux = 63  # 100% = efficace contre les coques les plus légères

cVsVaisseaux = 100
cVsStations = 10
cVsStructures = 0

cPv = 100
cBouclier = 100
cBlindage = 100
cEsquive = 100

lvlCinetique = 1
lvlLaser = 7
lvlChimique = 3
lvlRobotique = 5
lvlPv = 7
lvlBouclier = 3
lvlBlindage = 4
lvlEsquive = 3

nb = 4

wb = load_workbook('Nova.xlsx')
vaisseaux = wb['Vaisseaux']
armes = wb['Armes']
composants = wb['Composants']
recherches = wb['Recherches']


def arrang(n, k):
    """Nombre des arrangements de n objets pris k à k"""
    if k > n:
        return 0
    x = 1
    i = n - k + 1
    while i <= n:
        x *= i
        i += 1
    return x


totalArm = []
for col in range(nb):
    totalArm.append(0)
    for lig in range(11, 16):
        totalArm[col] += vaisseaux["BCDE"[col] + str(lig)].value

totalCom = []
for col in range(nb):
    totalCom.append(0)
    for lig in range(17, 22):
        totalCom[col] += vaisseaux["BCDE"[col] + str(lig)].value


def pTyp(typ, nbTyp):
    typArm = ["Cinétique " + typ, "Laser " + typ, "Chimique " + typ, "Robotique " + typ]
    return list(set(list(combinations(repeat(typArm, nbTyp), nbTyp))))


possArm = []
for col in range(nb):
    nbT = int(vaisseaux["BCDE"[col] + "11"].value)
    nbS = int(vaisseaux["BCDE"[col] + "12"].value)
    nbM = int(vaisseaux["BCDE"[col] + "13"].value)
    nbL = int(vaisseaux["BCDE"[col] + "14"].value)
    nbH = int(vaisseaux["BCDE"[col] + "15"].value)
    full = []
    if nbT != 0:
        full = pTyp("T", nbT)
        if nbS != 0:
            full = list(set(list(product(full, pTyp("S", nbS)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbM != 0:
            full = list(set(list(product(full, pTyp("M", nbM)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbL != 0:
            full = list(set(list(product(full, pTyp("L", nbL)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbH != 0:
            full = list(set(list(product(full, pTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbS != 0:
        full = pTyp("S", nbS)
        if nbM != 0:
            full = list(set(list(product(full, pTyp("M", nbM)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbL != 0:
            full = list(set(list(product(full, pTyp("L", nbL)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbH != 0:
            full = list(set(list(product(full, pTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbM != 0:
        full = pTyp("M", nbM)
        if nbL != 0:
            full = list(set(list(product(full, pTyp("L", nbL)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbH != 0:
            full = list(set(list(product(full, pTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbL != 0:
        full = pTyp("L", nbL)
        if nbH != 0:
            full = list(set(list(product(full, pTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbH != 0:
        full = pTyp("H", nbH)
    possArm.append(asarray(full))


def cTyp(typ, nbTyp):
    typCom = ["Blindage " + typ, "Cuirasse " + typ, "Bouclier " + typ, "Disrupteur " + typ, "Réacteur " + typ]
    return list(set(list(combinations(repeat(typCom, nbTyp), nbTyp))))


possCom = []
for col in range(nb):
    nbT = int(vaisseaux["BCDE"[col] + "17"].value)
    nbS = int(vaisseaux["BCDE"[col] + "18"].value)
    nbM = int(vaisseaux["BCDE"[col] + "19"].value)
    nbL = int(vaisseaux["BCDE"[col] + "20"].value)
    nbH = int(vaisseaux["BCDE"[col] + "21"].value)
    full = []
    if nbT != 0:
        full = cTyp("T", nbT)
        if nbS != 0:
            full = list(set(list(product(full, cTyp("S", nbS)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbM != 0:
            full = list(set(list(product(full, cTyp("M", nbM)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbL != 0:
            full = list(set(list(product(full, cTyp("L", nbL)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbH != 0:
            full = list(set(list(product(full, cTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbS != 0:
        full = cTyp("S", nbS)
        if nbM != 0:
            full = list(set(list(product(full, cTyp("M", nbM)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbL != 0:
            full = list(set(list(product(full, cTyp("L", nbL)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbH != 0:
            full = list(set(list(product(full, cTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbM != 0:
        full = cTyp("M", nbM)
        if nbL != 0:
            full = list(set(list(product(full, cTyp("L", nbL)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
        if nbH != 0:
            full = list(set(list(product(full, cTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbL != 0:
        full = cTyp("L", nbL)
        if nbH != 0:
            full = list(set(list(product(full, cTyp("H", nbH)))))
            for i in range(len(full)):
                full[i] = full[i][0] + full[i][1]
    elif nbH != 0:
        full = cTyp("H", nbH)
    possCom.append(asarray(full))

perVsVaisseaux = cVsVaisseaux / (cVsVaisseaux + cVsStations + cVsStructures)
perVsStations = cVsStations / (cVsVaisseaux + cVsStations + cVsStructures)
perVsStructures = cVsStructures / (cVsVaisseaux + cVsStations + cVsStructures)
A = "BCDEFGHIJKLMNOPQRSTU"
tabAConsommation = []
tabDegatsBase = []
tabAPrecision = []
tabVsVaisseaux = []
tabVsStations = []
tabVsStructures = []
tabPerfBlindage = []
tabPerfBouclier = []
tabDeltaVsVaisseaux = []
C = "BCDEFGHIJKLMNOPQRSTUVWXYZ"
tabCConsommation = []
tabCPv = []
tabCBouclier = []
tabCBlindage = []
tabCEsquive = []
V = "BCDE"
tabNom = []
tabVPrecision = []
tabVPv = []
tabVBouclier = []
tabVBlindage = []
tabVEsquive = []
R = "BCDEFGHIJKLMNOPQRSTUV"
bArm = [recherches[R[lvlCinetique] + "2"].value / 100, recherches[R[lvlLaser] + "3"].value / 100, recherches[R[lvlChimique] + "4"].value / 100, recherches[R[lvlRobotique] + "5"].value / 100]
bPv = recherches[R[lvlPv] + "7"].value / 100
bBouclier = recherches[R[lvlBouclier] + "8"].value / 100
bBlindage = recherches[R[lvlBlindage] + "9"].value / 100
bEsquive = recherches[R[lvlEsquive] + "10"].value / 100
for i in range(len(A)):
    tabAConsommation.append(armes[A[i] + "3"].value)
    tabDegatsBase.append(armes[A[i] + "4"].value / armes[A[i] + "6"].value)
    tabAPrecision.append(armes[A[i] + "5"].value / 100)
    tabVsVaisseaux.append(0)
    for j in range(10, 15):
        tabVsVaisseaux[i] += armes[A[i] + str(j)].value / 100
    tabVsVaisseaux[i] /= 5
    tabVsStations.append(armes[A[i] + "15"].value / 100)
    tabVsStructures.append(armes[A[i] + "16"].value / 100)
    tabPerfBlindage.append(armes[A[i] + "7"].value / 100)
    tabPerfBouclier.append(armes[A[i] + "8"].value / 100)
    tabDeltaVsVaisseaux.append((tabVsVaisseaux[i] - cVsVaisseaux) / cVsVaisseaux)
for i in range(len(C)):
    tabCConsommation.append(composants[C[i] + "3"].value)
    tabCPv.append(composants[C[i] + "5"].value)
    tabCBouclier.append(composants[C[i] + "6"].value)
    tabCBlindage.append(composants[C[i] + "4"].value / 100)
    tabCEsquive.append(composants[C[i] + "7"].value / 100)
for i in range(len(V)):
    tabNom.append(vaisseaux[V[i] + "1"].value)
    tabVPrecision.append(vaisseaux[V[i] + "5"].value / 100)
    tabVPv.append(vaisseaux[V[i] + "2"].value)
    tabVBouclier.append(vaisseaux[V[i] + "3"].value)
    tabVBlindage.append(vaisseaux[V[i] + "4"].value / 100)
    tabVEsquive.append(vaisseaux[V[i] + "6"].value / 100)


def AToInt(typArm):
    if typArm == "Cinétique":
        return 0
    elif typArm == "Laser":
        return 1
    elif typArm == "Chimique":
        return 2
    else:
        return 3


def CToInt(typArm):
    if typArm == "Blindage":
        return 0
    elif typArm == "Cuirasse":
        return 1
    elif typArm == "Bouclier":
        return 2
    elif typArm == "Disrupteur":
        return 3
    else:
        return 4


def TToInt(typ):
    if typ == "T":
        return 0
    elif typ == "S":
        return 1
    elif typ == "M":
        return 2
    elif typ == "L":
        return 3
    else:
        return 4


class Arm:
    def __init__(self, arms, typVaisseau, moy):
        self.arms = arms
        self.indiceArms = 0
        self.consommation = 0

        if not moy:
            for i in range(len(arms)):
                typArm = AToInt(arms[i].split(" ")[0])
                typ = TToInt(arms[i].split(" ")[1])
                idx = 5 * typArm + typ
                self.consommation += tabAConsommation[idx]
                perfBlindage = tabPerfBlindage[idx]
                perfBouclier = tabPerfBouclier[idx]
                indiceVsVaisseaux = (1 + 4 * perfBlindage / 5 + perfBouclier / 5) * (1 - tabDeltaVsVaisseaux[idx])
                indiceVsStations = (1 + perfBlindage / 2 + perfBouclier / 2) * tabVsStations[idx]
                indiceVsStructures = (1 + perfBlindage / 2 + perfBouclier / 2) * tabVsStructures[idx]

                self.indiceArms += (perVsVaisseaux * indiceVsVaisseaux + perVsStations * indiceVsStations + perVsStructures * indiceVsStructures) * tabDegatsBase[idx] * (1 + bArm[typArm]) * (tabAPrecision[idx] + tabVPrecision[typVaisseau])


class Com:
    def __init__(self, coms, typVaisseau, moy):
        self.coms = coms
        self.indiceComs = 0
        self.pv = 0
        self.bouclier = 0
        self.blindage = 0
        self.esquive = 0
        self.consommation = 0

        if not moy:
            perPv = cPv / (cPv + cBouclier)
            perBouclier = cBouclier / (cPv + cBouclier)
            for i in range(len(coms)):
                typArm = CToInt(coms[i].split(" ")[0])
                typ = TToInt(coms[i].split(" ")[1])
                idx = 5 * typArm + typ
                self.consommation += tabCConsommation[idx]
                self.pv += tabCPv[idx]
                self.bouclier += tabCBouclier[idx]
                self.blindage += tabCBlindage[idx]
                self.esquive += tabCEsquive[idx]
            self.pv += tabVPv[typVaisseau]
            self.pv *= (1 + bPv)
            self.bouclier += tabVBouclier[typVaisseau]
            self.bouclier *= (1 + bBouclier)
            self.blindage += tabVBlindage[typVaisseau] + bBlindage
            self.esquive += tabVEsquive[typVaisseau] + bEsquive
            self.indiceComs = self.pv * (1 + self.blindage * cBlindage / 100 + self.esquive * cEsquive / 100) * perPv + self.bouclier * (1 + self.esquive * cEsquive / 100) * perBouclier


class Ship:
    def __init__(self, arm, com):
        self.arms = arm.arms
        self.coms = com.coms

        self.indiceArms = arm.indiceArms
        self.indiceComs = com.indiceComs

        self.pv = com.pv
        self.bouclier = com.bouclier
        self.blindage = com.blindage
        self.esquive = com.esquive
        self.consommation = arm.consommation + com.consommation

    def create_ship(arm, com):
        consommation = arm.consommation + com.consommation
        if consommation < 0:
            return None
        return Ship(arm, com)


listShip = []
moy = []
for i in range(nb):
    listA = []
    listC = []
    listS = []
    for j in range(len(possArm[i])):
        listA.append(Arm(possArm[i][j], i, False))
    for k in range(len(possCom[i])):
        listC.append(Com(possCom[i][k], i, False))
    for j in range(len(listA)):
        for k in range(len(listC) - 1, -1, -1):
            tmp = Ship.create_ship(listA[j], listC[k])
            if tmp is not None:
                listS.append(tmp)
            else:
                del listC[k]  # Parce que toutes les consommations Armes sont les mêmes quelque soit les types d'armes
    moy.append(Ship(Arm(None, None, True), Com(None, None, True)))
    l = len(listS)
    for j in range(l):
        moy[i].indiceArms += listS[j].indiceArms
        moy[i].indiceComs += listS[j].indiceComs
        moy[i].pv += listS[j].pv
        moy[i].bouclier += listS[j].bouclier
        moy[i].blindage += listS[j].blindage
        moy[i].esquive += listS[j].esquive
        moy[i].consommation += listS[j].consommation
    moy[i].indiceArms /= l
    moy[i].indiceComs /= l
    moy[i].pv /= l
    moy[i].bouclier /= l
    moy[i].blindage /= l
    moy[i].esquive /= l
    moy[i].consommation /= l
    listShip.append(listS)

perShip = []
top = []
for i in range(nb):
    perA = []
    tmpTop = []
    for j in range(len(listShip[i])):
        r0 = listShip[i][j].arms
        r1 = listShip[i][j].coms
        r2 = listShip[i][j].indiceArms / moy[i].indiceArms
        r3 = listShip[i][j].indiceComs / moy[i].indiceComs
        r4 = listShip[i][j].pv
        r5 = listShip[i][j].bouclier
        r6 = listShip[i][j].blindage
        r7 = listShip[i][j].esquive
        r8 = listShip[i][j].consommation
        if len(tmpTop) < 5:
            tmpTop.append([(r2 + r3) / 2, r0, r1, r2, r3, r4, r5, r6, r7, r8])
            tmpTop.sort(key=lambda x: -x[0])
        elif tmpTop[len(tmpTop) - 1][0] < (r2 + r3) / 2:
            tmpTop[len(tmpTop) - 1] = [(r2 + r3) / 2, r0, r1, r2, r3, r4, r5, r6, r7, r8]
            tmpTop.sort(key=lambda x: -x[0])
    top.append(tmpTop)

for i in range(nb):
    print("-----  " + str(tabNom[i]) + "  -----  Moyenne  -----")
    print("Moyenne:            ")
    print("   - de dégats:     " + "%.1f" % moy[i].indiceArms)
    print("   - de compo.:     " + "%.1f" % moy[i].indiceComs)
    print("PV:                 " + "%.1f" % moy[i].pv)
    print("Bouclier:           " + "%.1f" % moy[i].bouclier)
    print("Blindage:           " + "%.1f" % (moy[i].blindage * 100) + "%")
    print("Esquive:            " + "%.1f" % (moy[i].esquive * 100) + "%")
    print("Consommation:       " + "%.1f" % moy[i].consommation)
    print("\n")
    for j in range(len(top[i])):
        print("-----  " + str(tabNom[i]) + "  -----  Top " + str(j + 1) + "  -----")
        print("Indice:             " + "%.2f" % (top[i][j][0] * 100) + "%")
        print("   - de dégats:     " + "%.2f" % (top[i][j][3] * 100) + "%")
        print("   - de compo.:     " + "%.2f" % (top[i][j][4] * 100) + "%")
        print("Armements:          " + str(top[i][j][1]))
        print("Composants:         " + str(top[i][j][2]))
        print("PV:                 " + "%.1f" % top[i][j][5])
        print("Bouclier:           " + "%.1f" % top[i][j][6])
        print("Blindage:           " + "%.1f" % (top[i][j][7] * 100) + "%")
        print("Esquive:            " + "%.1f" % (top[i][j][8] * 100) + "%")
        print("Consommation:       " + "%.1f" % top[i][j][9])
        print("\n")
    print("\n")
